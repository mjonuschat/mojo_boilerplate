page.boilerplate {
  enable {
    conditional_css = 1
    modernizr = 1
    base_css = 1
    short_charset = 1
    selectivizr = 0
    mediaqueries = 0
    jqueryui = 0
    jquery = 1
    chrome_frame = 0
    cdn = 1
  }
  meta {
    ua_compatible = IE=edge,chrome=1
    viewport = width=device-width
  }

  options {
    google_analytics = UA-XXXXX-X
  }
}
