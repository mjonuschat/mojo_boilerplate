<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2010-2012 Morton Jonuschat <yabawock@gmail.com>
 *  All rights reserved
 *
 *  This script is part of the mojo_sass TYPO3 extension. The
 *  mojo_sass extension is free software; you can redistribute
 *  it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation;
 *  either version 2 of the License, or (at your option) any
 *  later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *  A copy is found in the textfile GPL.txt and important notices
 *  to the license from the author is found in LICENSE.txt
 *  distributed with these scripts.
 *
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

class Tx_MojoBoilerplate_Hooks_BoilerplateHooks {
  /**
   * Extension configuration
   *
   * @var array
   */
  protected $extConfig;

  /**
   * Page configuration
   *
   * @var array
   */
  protected $pageConfig;

  /**
   * The page rendered for the current page
   *
   * @var t3lib_PageRenderer
   */
  protected $pageRenderer;

  /**
   * The current TYPO3 frontend object
   *
   * @var tslib_fe
   */
  protected $feObj;

  /**
   * Constructor for Tx_MojoBoilerplate_Hooks_BoilerplateHooks
   *
   * Retrieves the current page config and sets the default
   * config in case no configuration has been supplied before
   * invocation.
   *
   * @return void
   */
  public function __construct() {
    $this->extConfig = $GLOBALS['TSFE']->pSetup['boilerplate.'];
    $this->setFallbackConfig();
  }

  /**
   * Hook to do post processing of the rendered page.
   * Mostly used to set modified HTML Tags and ensure
   * Modernizr is added at the latest possible time
   * so it really gets positioned before any other
   * Javascript
   *
   * @param array $config
   * @param t3lib_PageRenderer $pageRenderer
   * @return void
   */
  public function renderPostProcess(&$config, t3lib_PageRenderer &$pageRenderer) {
    $this->pageConfig = &$config;
    $this->pageRenderer = &$pageRenderer;
    // Internet Explorer Conditional HTML Tag
    $this->addConditionalHtmlTag();
    // HTML5 Charset Tag
    $this->addCharsetTag();
    // Add Modernizr to the header
    $this->addModernizr();
  }

  /**
   * Hook to post process the page configuration
   *
   * @param array $config
   * @param tslib_fe $feObj
   * @return void
   */
  public function configArrayPostProc(&$config, tslib_fe &$feObj) {
    $this->feObj = &$feObj;
    // Add information to the page header
    $this->addBaseCss();
    $this->addMetaTags();
    // Add Javascripts loaded with yepnope to parallelize downloads
    $this->addJQuery();
    $this->addSelectivizr();
    $this->addMediaQueries();
    // Append the google analytics code to the inline JS
    $this->addGoogleAnalytics();
  }

  /**
   * Include the normalize CSS included with the original
   * HTML5 boilerplate. This tries to make all browser
   * behave the same without any other stylesheet applied
   *
   * @return void
   */
  protected function addBaseCss() {
    if(intval($this->extConfig['enable.']['base_css']) == 1) {
      $baseCss = array(
        'mojo_boilerplate_base' => 'EXT:mojo_boilerplate/Resources/Public/Stylesheets/html5boilerplate-base.css',
        'mojo_boilerplate_base.' => array(
          'forceOnTop' => 1,
          'compress' => 1,
        )
      );
      if(is_array($this->feObj->pSetup['includeCSS.'])) {
        $this->feObj->pSetup['includeCSS.'] = $baseCss + $this->feObj->pSetup['includeCSS.'];
      }

      $this->feObj->pSetup['includeCSS.']['mojo_boilerplate_helpers'] = 'EXT:mojo_boilerplate/Resources/Public/Stylesheets/html5boilerplate-helpers.css';
      $this->feObj->pSetup['includeCSS.']['mojo_boilerplate_helpers.']['compress'] = 1;
    }
  }

  /**
   * Switch the charset meta tag to the short notation
   * advocated by the W3C
   *
   * @return void
   */
  protected function addCharsetTag() {
    if(intval($this->extConfig['enable.']['short_charset']) == 1) {
      $this->pageConfig['metaCharsetTag'] = '<meta charset="|">';
    }
  }

  /**
   * Add the viewport meta tag to the page header
   *
   * @return void
   */
  protected function addMetaTags() {
    if($this->extConfig['meta.']['viewport'] != '') {
      $this->feObj->pSetup['meta.']['viewport'] = trim($this->extConfig['meta.']['viewport']);
    }
  }

  /**
   * Add Modernizr to the page header. Browser feature detection and HTML5
   * enabling for IE6-8
   *
   * @return void
   */
  protected function addModernizr() {
    if(intval($this->extConfig['enable.']['modernizr']) == 1) {
      $this->pageConfig['jsLibs'] = '<script src="' . t3lib_extMgm::siteRelPath('mojo_boilerplate') . 'Resources/Public/JavaScripts/Libs/modernizr/modernizr.min.js" type="text/javascript"></script>';
    }
  }

  /**
   * Add polyfill to support a set of CSS2.1 and 3 selectors
   * the IE6-8 don't support natively
   *
   * @return void
   */
  protected function addSelectivizr() {
    if(intval($this->extConfig['enable.']['selectivizr']) == 1) {
      $this->feObj->pSetup['includeJSFooter.']['nwmatcher'] = 'EXT:mojo_boilerplate/Resources/Public/JavaScripts/Polyfills/nwmatcher.min.js';
      $this->feObj->pSetup['includeJSFooter.']['nwmatcher.'] = array(
        'allWrap' => '<!--[ if lt IE 9 ]>|<![endif]-->',
        'disableCompression' => 1,
        'excludeFromConcatenation' => 1,
      );
      $this->feObj->pSetup['includeJSFooter.']['selectivizr'] = 'EXT:mojo_boilerplate/Resources/Public/JavaScripts/Polyfills/selectivizr.min.js';
      $this->feObj->pSetup['includeJSFooter.']['selectivizr.'] = array(
        'allWrap' => '<!--[ if lt IE 9 ]>|<![endif]-->',
        'disableCompression' => 1,
        'excludeFromConcatenation' => 1,
      );
    }
  }

  /**
   * Add polyfill to support mediaqueries on IE6-8
   *
   * @return void
   */
  protected function addMediaQueries() {
    if(intval($this->extConfig['enable.']['mediaqueries']) == 1) {
      $this->feObj->pSetup['includeJSFooter.']['mediaqueries'] = 'EXT:mojo_boilerplate/Resources/Public/JavaScripts/Polyfills/css3-mediaqueries.min.js';
      $this->feObj->pSetup['includeJSFooter.']['mediaqueries.'] = array(
        'allWrap' => '<!--[if lt IE 9 ]>|<!endif]-->',
        'disableCompression' => 1,
        'excludeFromConcatenation' => 1,
      );
    }
  }

  /**
   * Add jQuery to the array of javascript libraries
   * included in this page. Has a fallback to the local
   * copy if the CDN doesn't respond.
   *
   * If t3jquery is in use for this page we honor it
   * and use their provisioning
   *
   * @return void
   */
  protected function addJQuery() {
    # Honor t3jQuery settings if they are available
    if (t3lib_extMgm::isLoaded('t3jquery')) {
      require_once(t3lib_extMgm::extPath('t3jquery') . 'class.tx_t3jquery.php');
    }
    if (T3JQUERY === true) {
      tx_t3jquery::addJqJS();
    } else {
      # Include our own jquery setup
      if(intval($this->extConfig['enable.']['jquery']) == 1) {
        # Include jquery from google content distribution network
        if(intval($this->extConfig['enable.']['cdn']) == 1) {
          $this->feObj->pSetup['includeJSFooterlibs.']['jquery'] = '//ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js';
          $this->feObj->pSetup['includeJSFooterlibs.']['jquery.'] = array(
            'external' => 1,
            'disableCompression' => 1,
            'excludeFromConcatenation' => 1,
            'allWrap' => "|\n<script>if(!window.jQuery) { document.write('<script src=\"" . $GLOBALS['TSFE']->tmpl->getFileName('EXT:mojo_boilerplate/Resources/Public/JavaScripts/Libs/jquery/jquery.min.js') . "\">\\x3C/script>')}</script>"
          );
        }
        # use local jquery copy
        else {
          $this->feObj->pSetup['includeJSFooterlibs.']['jquery'] = 'EXT:mojo_boilerplate/Resources/Public/JavaScripts/Libs/jquery/jquery.min.js';
        }
        $this->addJQueryUI();
      }
    }
  }

  /**
   * Add jQuery UI to the array of javascript libraries
   * included in this page. Has a fallback to the local
   * copy if the CDN doesn't respond
   *
   * @return void
   */
  protected function addJQueryUI() {
    if(intval($this->extConfig['enable.']['jqueryui']) == 1) {
      # Include jqueryui from google content distribution network
      if(intval($this->extConfig['enable.']['cdn']) == 1) {
        $this->feObj->pSetup['includeJSFooterlibs.']['jqueryui'] = '//ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js';
        $this->feObj->pSetup['includeJSFooterlibs.']['jqueryui.'] = array(
          'external' => 1,
          'disableCompression' => 1,
          'excludeFromConcatenation' => 1,
          'allWrap' => "|\n<script>if(!window.jQuery.ui) { document.write('<script src=\"" . $GLOBALS['TSFE']->tmpl->getFileName('EXT:mojo_boilerplate/Resources/Public/JavaScripts/Libs/jquery/jquery-ui.min.js') . "\">\\x3C/script>')}</script>"
        );
      }
      else {
        # use local jqueryui copy
        $this->feObj->pSetup['includeJSFooterlibs.']['jqueryui'] = 'EXT:mojo_boilerplate/Resources/Public/JavaScripts/Libs/jquery/jquery-ui.min.js';
      }
    }
  }

  /**
   * Prompt users of IE6 to install the Google Chrome Frame
   * for a modern browsing experience
   *
   * @return void
   */
  protected function addChromeFrame() {
    if(intval($this->extConfig['enable.']['chrome_frame']) == 1) {
      $this->feObj->pSetup['includeJSFooter.']['chromeframe'] = '//ajax.googleapis.com/ajax/libs/chrome-frame/1.0/CFInstall.min.js';
      $this->feObj->pSetup['includeJSFooter.']['chromeframe.'] = array(
        'external' => 1,
        'disableCompression' => 1,
        'excludeFromConcatenation' => 1,
        'allWrap' => "<!--[if lt IE 7 ]>|<script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script><!endif]-->"
      );
    }
  }

  /**
   * Change the current htmlTag to the conditional HTML tag recommended
   * by the HTML5 boilerplate. Allows for scoping CSS by browser (for IE)
   *
   * @return void
   */
  protected function addConditionalHtmlTag() {
    if(intval($this->extConfig['enable.']['conditional_css']) == 1) {
      if(preg_match('#class="[^"]"#', $this->pageConfig['htmlTag'])) {
        $baseHtmlTag = preg_replace('#class="[^"]"#', 'class="\1|"', $this->pageConfig['htmlTag']);
      } else {
        $baseHtmlTag = preg_replace('#(\/?>)$#', ' class="|"\1', $this->pageConfig['htmlTag']);
      }

      $this->pageConfig['htmlTag']  = sprintf("<!--[if lt IE 7 ]> %s <![endif]-->\n", str_replace('|', 'no-js lt-ie9 lt-ie8 lt-ie7', $baseHtmlTag));
      $this->pageConfig['htmlTag'] .= sprintf("<!--[if IE 7 ]>    %s <![endif]-->\n", str_replace('|', 'no-js lt-ie9 lt-ie8', $baseHtmlTag));
      $this->pageConfig['htmlTag'] .= sprintf("<!--[if IE 8 ]>    %s <![endif]-->\n", str_replace('|', 'no-js lt-ie9', $baseHtmlTag));
      $this->pageConfig['htmlTag'] .= sprintf("<!--[if gt IE 8]><!--> %s <!--<![endif]-->\n", str_replace('|', 'no-js', $baseHtmlTag));
    }
  }

  /**
   * Add the asynchronous Google Analytics Snippet to the page footer
   *
   * @return void
   */
  protected function addGoogleAnalytics() {
    if($this->extConfig['options.']['google_analytics'] == 'UA-XXXXX-X' || trim($this->extConfig['options.']['google_analytics']) == '') {
      return;
    }
    $this->feObj->pSetup['jsFooterInline.'][1] = 'TEXT';
    $this->feObj->pSetup['jsFooterInline.']['1.']['value'] .= "
      var _gaq = [['_setAccount', '" . $this->extConfig['options.']['google_analytics'] . "'], ['_trackPageview']];
      (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
      g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
      s.parentNode.insertBefore(g,s)}(document,'script'));
    ";
  }

  /**
   * If not a single config option has been given for boilerplate
   * set the default/recommended settings to work with.
   *
   * @return void
   */
  protected function setFallbackConfig() {
    if(!is_array($this->extConfig)) {
      $this->extConfig = array(
        'enable' => array(
          'conditional_css' => 1,
          'modernizr' => 1,
          'base_css' => 1,
          'short_charset' => 1,
          'selectivizr' => 0,
          'mediaqueries' => 0,
          'jqueryui' => 0,
          'jquery' => 1,
          'chrome_frame' => 0,
        ),
        'external' => array(
          'jqueryui' => 1,
          'jquery' => 1,
        ),
        'meta' => array(
          'ua_compatible' => 'IE=edge,chrome=1',
          'viewport' => 'width=device-width',
        ),
        'options' => array(
          'google_analytics' => 'UA-XXXXX-X',
        )
      );
    }
  }
}
