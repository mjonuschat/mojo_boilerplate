<?php

########################################################################
# Extension Manager/Repository config file for ext "mojo_sass".
#
# Auto generated 16-11-2010 08:57
#
# Manual updates:
# Only the data in the array - everything else is removed by next
# writing. "version" and "dependencies" must not be touched!
########################################################################

$EM_CONF[$_EXTKEY] = array(
  'title' => 'HTML5 Boilerplate',
  'description' => 'A rock-solid default for HTML5 awesome',
  'category' => 'fe',
  'author' => 'Morton Jonuschat',
  'author_email' => 'yabawock@gmail.com',
  'shy' => '',
  'dependencies' => '',
  'conflicts' => '',
  'priority' => '',
  'module' => '',
  'state' => 'stable',
  'internal' => '',
  'uploadfolder' => 0,
  'createDirs' => '',
  'modify_tables' => '',
  'clearCacheOnLoad' => 1,
  'lockType' => '',
  'author_company' => '',
  'version' => '3.0.4',
  'constraints' => array(
    'depends' => array(
    ),
    'conflicts' => array(
    ),
    'suggests' => array(
      't3jquery' => '2.0.7-0.0.0',
    ),
  ),
  '_md5_values_when_last_written' => 'a:5:{s:9:"ChangeLog";s:4:"2021";s:10:"README.txt";s:4:"ee2d";s:12:"ext_icon.gif";s:4:"1bdc";s:19:"doc/wizard_form.dat";s:4:"e5b1";s:20:"doc/wizard_form.html";s:4:"cfee";}',
);

?>
